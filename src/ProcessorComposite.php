<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ProcessorComposite;

use Interactiv4\Contracts\DataObject\Api\DataObjectInterface;
use Interactiv4\Contracts\Processor\Exception\ProcessorException;
use Interactiv4\ProcessorComposite\Api\ProcessorCompositeInterface;
use Interactiv4\ProcessorComposite\Api\ProcessorDefinitionInterface;

/**
 * Class ProcessorComposite.
 *
 * @api
 */
class ProcessorComposite implements ProcessorCompositeInterface
{
    /**
     * @var ProcessorDefinitionInterface[]
     */
    private $processorDefinitions;

    /**
     * ProcessorComposite constructor.
     *
     * @param ProcessorDefinitionInterface[] $processorDefinitions
     */
    public function __construct(
        array $processorDefinitions = []
    ) {
        $this->processorDefinitions = $processorDefinitions;
    }

    /**
     * Composite processor.
     *
     * {@inheritdoc}
     */
    public function process(?DataObjectInterface $data = null): void
    {
        $this->checkProcessorDefinitions();
        $this->sortProcessors();

        foreach ($this->processorDefinitions as $processorDefinition) {
            try {
                $processorDefinition->getProcessor()->process($data);
            } catch (ProcessorException $e) {
                switch ($processorDefinition->getExceptionHandlingMode()) {
                    case ProcessorDefinitionInterface::EXCEPTION_HANDLING_MODE_PROPAGATE_EXCEPTION:
                        throw new ProcessorException($e->getMessage(), $e->getCode(), $e);
                        break;
                    case ProcessorDefinitionInterface::EXCEPTION_HANDLING_MODE_STOP_GRACEFUL:
                        break 2;
                    case ProcessorDefinitionInterface::EXCEPTION_HANDLING_MODE_IGNORE:
                        break;
                    default:
                        $errorMessage = \sprintf(
                            'Unsupported exception handling mode %s',
                            $processorDefinition->getExceptionHandlingMode()
                        );
                        throw new ProcessorException($errorMessage);
                }
            }
        }
    }

    /**
     * Check processors definition.
     *
     * @throws ProcessorException
     */
    private function checkProcessorDefinitions(): void
    {
        foreach ($this->processorDefinitions as $processorDefinition) {
            if (!\is_object($processorDefinition)) {
                $errorMessage = \sprintf(
                    'Object expected, %s given',
                    \gettype($processorDefinition)
                );

                throw new ProcessorException($errorMessage);
            }

            if (!$processorDefinition instanceof ProcessorDefinitionInterface) {
                $errorMessage = \sprintf(
                    '%s expected, %s given',
                    ProcessorDefinitionInterface::class,
                    \get_class($processorDefinition)
                );

                throw new ProcessorException($errorMessage);
            }
        }
    }

    /**
     * Sort processors by priority DESC.
     */
    private function sortProcessors(): void
    {
        \uasort(
            $this->processorDefinitions,
            function (
                $firstItem,
                $secondItem
            ) {
                /* @var ProcessorDefinitionInterface $firstItem */
                /* @var ProcessorDefinitionInterface $secondItem */
                return $firstItem->getPriority() < $secondItem->getPriority();
            }
        );
    }
}
