<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ProcessorComposite;

use Interactiv4\Contracts\Processor\Api\ProcessorInterface;
use Interactiv4\ProcessorComposite\Api\ProcessorDefinitionInterface;

/**
 * Class ProcessorDefinition.
 *
 * @api
 */
class ProcessorDefinition implements ProcessorDefinitionInterface
{
    /**
     * @var ProcessorInterface
     */
    private $processor;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int|null
     */
    private $priority;

    /**
     * @var int|null
     */
    private $exceptionHandlingMode;

    /**
     * ProcessorDefinition constructor.
     *
     * @param ProcessorInterface $processor
     * @param string|null        $name
     * @param int|null           $priority
     * @param int|null           $exceptionHandlingMode
     */
    public function __construct(
        ProcessorInterface $processor,
        string $name = null,
        int $priority = null,
        int $exceptionHandlingMode = null
    ) {
        $this->processor = $processor;
        $this->name = $name;
        $this->priority = $priority;
        $this->exceptionHandlingMode = $exceptionHandlingMode;
    }

    /**
     * {@inheritdoc}
     */
    public function getProcessor(): ProcessorInterface
    {
        return $this->processor;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return $this->name ?? \get_class($this->processor);
    }

    /**
     * {@inheritdoc}
     */
    public function getPriority(): int
    {
        return $this->priority ?? 0;
    }

    /**
     * {@inheritdoc}
     */
    public function getExceptionHandlingMode(): int
    {
        return $this->exceptionHandlingMode
            ?? ProcessorDefinitionInterface::EXCEPTION_HANDLING_MODE_PROPAGATE_EXCEPTION;
    }
}
