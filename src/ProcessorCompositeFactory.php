<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ProcessorComposite;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;

/**
 * Class ProcessorCompositeFactory.
 *
 * @api
 */
class ProcessorCompositeFactory implements FactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function create(array $data = []): ProcessorComposite
    {
        $processorDefinitions = $data['processorDefinitions'] ?? [];

        return new ProcessorComposite($processorDefinitions);
    }
}
