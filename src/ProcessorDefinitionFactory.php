<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ProcessorComposite;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;

/**
 * Class ProcessorDefinitionFactory.
 *
 * @api
 */
class ProcessorDefinitionFactory implements FactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function create(array $data = []): ProcessorDefinition
    {
        $processor = $data['processor'] ?? null;
        $name = $data['name'] ?? null;
        $priority = $data['priority'] ?? null;
        $exceptionHandlingMode = $data['exceptionHandlingMode'] ?? null;

        return new ProcessorDefinition($processor, $name, $priority, $exceptionHandlingMode);
    }
}
