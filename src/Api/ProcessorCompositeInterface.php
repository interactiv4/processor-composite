<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ProcessorComposite\Api;

use Interactiv4\Contracts\Processor\Api\ProcessorInterface;

/**
 * Interface ProcessorCompositeInterface.
 *
 * Marker interface for composite processors.
 *
 * @api
 */
interface ProcessorCompositeInterface extends ProcessorInterface
{
}
