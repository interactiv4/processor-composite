<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ProcessorComposite\Api;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;
use Interactiv4\Contracts\Factory\Api\ObjectFactoryInterface;
use Interactiv4\Contracts\Factory\Factory\Proxy;
use Interactiv4\ProcessorComposite\ProcessorDefinitionFactory;

/**
 * Class ProcessorDefinitionInterfaceFactory.
 *
 * @api
 */
class ProcessorDefinitionInterfaceFactory extends Proxy implements FactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function __construct(
        ObjectFactoryInterface $objectFactory,
        string $factoryClass = ProcessorDefinitionFactory::class,
        array $factoryArguments = []
    ) {
        parent::__construct(
            $objectFactory,
            $factoryClass,
            $factoryArguments
        );
    }

    /**
     * Proxy object creation to concrete factory.
     *
     * {@inheritdoc}
     */
    public function create(array $arguments = []): ProcessorDefinitionInterface
    {
        return parent::create($arguments);
    }
}
