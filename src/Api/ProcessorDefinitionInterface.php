<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ProcessorComposite\Api;

use Interactiv4\Contracts\Processor\Api\ProcessorInterface;

/**
 * Interface ProcessorCompositeInterface.
 *
 * @api
 */
interface ProcessorDefinitionInterface
{
    const EXCEPTION_HANDLING_MODE_STOP_GRACEFUL = 1;

    const EXCEPTION_HANDLING_MODE_PROPAGATE_EXCEPTION = 2;

    const EXCEPTION_HANDLING_MODE_IGNORE = 3;

    /**
     * Get processor instance.
     *
     * @return ProcessorInterface
     */
    public function getProcessor(): ProcessorInterface;

    /**
     * Get processor name.
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Get processing priority.
     *
     * @return int
     */
    public function getPriority(): int;

    /**
     * Get exception handling mode.
     *
     * @return int
     */
    public function getExceptionHandlingMode(): int;
}
