<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\ProcessorComposite\Test;

use Interactiv4\Contracts\Processor\Api\ProcessorInterface;
use Interactiv4\Contracts\Processor\Exception\ProcessorException;
use Interactiv4\DataObject\DataObject;
use Interactiv4\ProcessorComposite\Api\ProcessorDefinitionInterface;
use Interactiv4\ProcessorComposite\ProcessorComposite;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class ProcessorCompositeTest.
 *
 * @internal
 */
class ProcessorCompositeTest extends TestCase
{
    /**
     * Test ProcessorComposite class exists and is an instance of ProcessorInterface.
     */
    public function testInstanceOf(): void
    {
        $processor = new ProcessorComposite();
        static::assertInstanceOf(ProcessorInterface::class, $processor);
    }

    /**
     * Test processing order.
     *
     * @param int   $expectedPriorityA
     * @param int   $expectedPriorityB
     * @param int   $expectedPriorityC
     * @param array $expectedResult
     * @dataProvider processingOrderDataProvider
     */
    public function testProcessingOrder(
        int $expectedPriorityA,
        int $expectedPriorityB,
        int $expectedPriorityC,
        array $expectedResult
    ): void {
        /** @var DataObject $dataObject */
        $dataObject = new DataObject(['processor_keys' => []]);

        $processor = new ProcessorComposite(
            [
                $this->getConfiguredProcessorDefinitionInterfaceMock($dataObject, 'A', 1, $expectedPriorityA),
                $this->getConfiguredProcessorDefinitionInterfaceMock($dataObject, 'B', 1, $expectedPriorityB),
                $this->getConfiguredProcessorDefinitionInterfaceMock($dataObject, 'C', 1, $expectedPriorityC),
            ]
        );

        $processor->process($dataObject);

        // Test processor sorting, so they are processed in correct order based on sort order
        static::assertSame($expectedResult, $dataObject->getData('processor_keys'));
    }

    /**
     * @return array
     */
    public function processingOrderDataProvider(): array
    {
        return [
            'same_priority_default' => [0, 0, 0, ['A', 'B', 'C']],
            'same_priority_defined' => [100, 100, 100, ['A', 'B', 'C']],
            'straight_priority_defined' => [30, 20, 10, ['A', 'B', 'C']],
            'inverse_priority_defined' => [5, 20, 100, ['C', 'B', 'A']],
            'custom_order_1' => [5, 20, 10, ['B', 'C', 'A']],
            'custom_order_2' => [50, 20, 100, ['C', 'A', 'B']],
        ];
    }

    /**
     * Test exception handling.
     *
     * @param bool   $expectedExceptionA
     * @param bool   $expectedExceptionB
     * @param bool   $expectedExceptionC
     * @param int    $expectedInvocationCountA
     * @param int    $expectedInvocationCountB
     * @param int    $expectedInvocationCountC
     * @param int    $expectedHandlingModeA
     * @param int    $expectedHandlingModeB
     * @param int    $expectedHandlingModeC
     * @param bool   $expectException
     * @param string $expectedExceptionMessage
     * @param array  $expectedResult
     *
     * @dataProvider exceptionHandlingDataProvider
     */
    public function testExceptionHandling(
        bool $expectedExceptionA,
        bool $expectedExceptionB,
        bool $expectedExceptionC,
        int $expectedInvocationCountA,
        int $expectedInvocationCountB,
        int $expectedInvocationCountC,
        int $expectedHandlingModeA,
        int $expectedHandlingModeB,
        int $expectedHandlingModeC,
        bool $expectException,
        string $expectedExceptionMessage = '',
        array $expectedResult = []
    ): void {
        /** @var DataObject $dataObject */
        $dataObject = new DataObject(['processor_keys' => []]);

        $processorMockDefinitionA = $this->getConfiguredProcessorDefinitionInterfaceMock($dataObject, 'A', $expectedInvocationCountA, 0, $expectedExceptionA);
        $processorMockDefinitionA->expects(static::any())->method('getExceptionHandlingMode')->willReturn($expectedHandlingModeA);
        $processorMockDefinitionB = $this->getConfiguredProcessorDefinitionInterfaceMock($dataObject, 'B', $expectedInvocationCountB, 0, $expectedExceptionB);
        $processorMockDefinitionB->expects(static::any())->method('getExceptionHandlingMode')->willReturn($expectedHandlingModeB);
        $processorMockDefinitionC = $this->getConfiguredProcessorDefinitionInterfaceMock($dataObject, 'C', $expectedInvocationCountC, 0, $expectedExceptionC);
        $processorMockDefinitionC->expects(static::any())->method('getExceptionHandlingMode')->willReturn($expectedHandlingModeC);

        $processor = new ProcessorComposite(
            [
                $processorMockDefinitionA,
                $processorMockDefinitionB,
                $processorMockDefinitionC,
            ]
        );

        if ($expectException) {
            $this->expectException(ProcessorException::class);
            $this->expectExceptionMessage($expectedExceptionMessage);
        }

        $processor->process($dataObject);

        // Test processor sorting, so they are processed in correct order based on sort order
        static::assertSame($expectedResult, $dataObject->getData('processor_keys'));
    }

    /**
     * @return array
     */
    public function exceptionHandlingDataProvider(): array
    {
        $stopGraceful = ProcessorDefinitionInterface::EXCEPTION_HANDLING_MODE_STOP_GRACEFUL;
        $propagate = ProcessorDefinitionInterface::EXCEPTION_HANDLING_MODE_PROPAGATE_EXCEPTION;
        $ignore = ProcessorDefinitionInterface::EXCEPTION_HANDLING_MODE_IGNORE;

        return [
            // Ok
            [false, false, false, 1, 1, 1, 0, 0, 0, false, '', ['A', 'B', 'C']],
            [false, false, false, 1, 1, 1, 4, 4, 4, false, '', ['A', 'B', 'C']],

            // Unsupported exception handling mode
            [true, false, false, 1, 0, 0, 0, 0, 0, true, 'Unsupported exception handling mode 0'],
            [true, false, false, 1, 0, 0, 997, 998, 999, true, 'Unsupported exception handling mode 997'],
            [false, true, false, 1, 1, 0, 0, 0, 0, true, 'Unsupported exception handling mode 0'],
            [false, true, false, 1, 1, 0, 997, 998, 999, true, 'Unsupported exception handling mode 998'],
            [false, false, true, 1, 1, 1, 0, 0, 0, true, 'Unsupported exception handling mode 0'],
            [false, false, true, 1, 1, 1, 997, 998, 999, true, 'Unsupported exception handling mode 999'],

            // Propagate exception
            [true, false, false, 1, 0, 0, $propagate, $propagate, $propagate, true, 'Error processing A'],
            [false, true, false, 1, 1, 0, $propagate, $propagate, $propagate, true, 'Error processing B'],
            [false, false, true, 1, 1, 1, $propagate, $propagate, $propagate, true, 'Error processing C'],

            [true, false, false, 1, 0, 0, $propagate, $ignore, $ignore, true, 'Error processing A'],
            [false, true, false, 1, 1, 0, $ignore, $propagate, $ignore, true, 'Error processing B'],
            [false, false, true, 1, 1, 1, $ignore, $ignore, $propagate, true, 'Error processing C'],

            [true, false, false, 1, 0, 0, $propagate, $stopGraceful, $stopGraceful, true, 'Error processing A'],
            [false, true, false, 1, 1, 0, $stopGraceful, $propagate, $stopGraceful, true, 'Error processing B'],
            [false, false, true, 1, 1, 1, $stopGraceful, $stopGraceful, $propagate, true, 'Error processing C'],

            // Stop graceful
            [true, false, false, 1, 0, 0, $stopGraceful, $stopGraceful, $stopGraceful, false, '', []],
            [false, true, false, 1, 1, 0, $stopGraceful, $stopGraceful, $stopGraceful, false, '', ['A']],
            [false, false, true, 1, 1, 1, $stopGraceful, $stopGraceful, $stopGraceful, false, '', ['A', 'B']],

            [true, false, false, 1, 0, 0, $stopGraceful, $propagate, $propagate, false, '', []],
            [false, true, false, 1, 1, 0, $propagate, $stopGraceful, $propagate, false, '', ['A']],
            [false, false, true, 1, 1, 1, $propagate, $propagate, $stopGraceful, false, '', ['A', 'B']],

            [true, false, false, 1, 0, 0, $stopGraceful, $ignore, $ignore, false, '', []],
            [false, true, false, 1, 1, 0, $ignore, $stopGraceful, $ignore, false, '', ['A']],
            [false, false, true, 1, 1, 1, $ignore, $ignore, $stopGraceful, false, '', ['A', 'B']],

            // Ignore exceptions
            [true, false, false, 1, 1, 1, $ignore, $ignore, $ignore, false, '', ['B', 'C']],
            [false, true, false, 1, 1, 1, $ignore, $ignore, $ignore, false, '', ['A', 'C']],
            [false, false, true, 1, 1, 1, $ignore, $ignore, $ignore, false, '', ['A', 'B']],
            [true, false, true, 1, 1, 1, $ignore, $ignore, $ignore, false, '', ['B']],
        ];
    }

    /**
     * Check invalid definitions.
     *
     * @param array  $processorDefinitions
     * @param string $expectedExceptionMessage
     *
     * @dataProvider invalidProcessorDefinitionsDataProvider
     */
    public function testInvalidProcessorDefinitions(
        array $processorDefinitions,
        string $expectedExceptionMessage
    ): void {
        $this->expectException(ProcessorException::class);
        $this->expectExceptionMessage($expectedExceptionMessage);

        $processor = new ProcessorComposite($processorDefinitions);
        $processor->process();
    }

    /**
     * @return array
     */
    public function invalidProcessorDefinitionsDataProvider(): array
    {
        return [
            'string' => [
                [
                    'processorA',
                ],
                'Object expected, string given',
            ],
            'int' => [
                [
                    1234,
                ],
                'Object expected, integer given',
            ],
            'object' => [
                [
                    new \stdClass(),
                ],
                ProcessorDefinitionInterface::class . ' expected, stdClass given',
            ],
        ];
    }

    /**
     * @param DataObject $dataObject
     * @param string     $processingValue
     * @param int        $expectedInvocationCount
     * @param int        $expectedPriority
     * @param bool       $throwException
     *
     * @return ProcessorDefinitionInterface|MockObject
     */
    private function getConfiguredProcessorDefinitionInterfaceMock(
        DataObject $dataObject,
        string $processingValue,
        int $expectedInvocationCount = 1,
        int $expectedPriority = 0,
        bool $throwException = false
    ) {
        /** @var ProcessorInterface|MockObject $processorMock */
        $processorMock = $this->getMockForAbstractClass(ProcessorInterface::class);

        $invocationMocker = $processorMock->expects(static::exactly($expectedInvocationCount))
            ->method('process')
            ->with($dataObject)
        ;

        if (!$throwException) {
            $invocationMocker->willReturnCallback(
                function ($dataObject) use ($processingValue): void {
                    /** @var DataObject $dataObject */
                    $processorKeys = $dataObject->getData('processor_keys');
                    $processorKeys[] = $processingValue;
                    $dataObject->setData('processor_keys', $processorKeys);
                }
            )
            ;
        }

        if ($throwException) {
            $invocationMocker->willThrowException(new ProcessorException('Error processing ' . $processingValue));
        }

        /** @var ProcessorDefinitionInterface|MockObject $processorDefinitionMock */
        $processorDefinitionMock = $this->getMockForAbstractClass(ProcessorDefinitionInterface::class);
        $processorDefinitionMock->expects(static::any())->method('getProcessor')->willReturn($processorMock);
        $processorDefinitionMock->expects(static::any())->method('getPriority')->willReturn($expectedPriority);

        return $processorDefinitionMock;
    }
}
